<?php

namespace App\Cells\Facades;

use Illuminate\Support\Facades\Facade;

class CellsFacade extends Facade {

    protected static function getFacadeAccessor() { return 'cells'; }

}