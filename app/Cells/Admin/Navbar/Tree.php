<?php

namespace App\Cells\Admin\Navbar;

use App\Lib\Singleton;
use App\Lib\Hierarchy\Tree as BaseTree;

class Tree extends BaseTree
{
    protected static $instance = null;

    const LOCALE_SCOPE = null;
    const STRUCT_PATH  = 'static_data/menu/admin/navbar.yaml';


    private static function node_url($node)
    {
//        print_r(route( $node->url['route'], $node->url['params'] ));
//        exit;
        $url = isset( $node->url['route'] ) ? route( $node->url['route'], $node->url['params'] ) :
            (isset( $node->url['link'] ) ? $node->url['link'] : null);

        return $url;
    }


    private static function node_title($node)
    {
        $title = isset( $node->title ) ? $node->title : null;
        if ( isset( $node->title_method ) ) {
            $method = $node->title_method;
            $title  = static::$method();
        }

        return $title;
    }

    private static function current_user_name() {
        return Auth::user()->name;
    }
}
