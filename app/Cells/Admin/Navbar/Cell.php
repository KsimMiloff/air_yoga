<?php

namespace App\Cells\Admin\Navbar;

use App\Cells\BaseCell;
use App\Cells\Admin\Navbar\Tree;

class Cell extends BaseCell {



    function show()
    {
        $root_node = Tree::root();

        return $this->view('show', [
            'root_node' => $root_node
        ]);
    }

    function left( $left_node )
    {

        if ( isset( $left_node ) )
        {
            return $this->view('menu', [
                'node'  => $left_node,
                'class' => 'nav navbar-nav'
            ]);
        }

    }

    function menu_item($node)
    {

        $li_class = [];
        $a_conf   = [];

        if ( count( $node->branches ) )
        {
            $li_class[]= $node->level() > 2 ? 'dropdown-submenu' : 'dropdown';

            $a_conf[]= 'class="dropdown-toggle" data-toggle="dropdown"';
        }

        $li_class = implode(' ', $li_class);
        $a_conf   = implode(' ', $a_conf);


        return $this->view('item', [
            'node'  => $node,
            'url'   => $this->node_url( $node ),
            'title' => $this->node_title( $node ),
            'li_class' => $li_class,
            'a_conf'   => $a_conf
        ]);
    }



    private static function node_url($node)
    {
        $url = isset( $node->url['route'] ) ? route( $node->url['route'], $node->url['params'] ) :
            (isset( $node->url['link'] ) ? $node->url['link'] : null);

        return $url;
    }


    private static function node_title($node)
    {
        $title = isset( $node->title ) ? $node->title : null;
        if ( isset( $node->title_method ) ) {
            $method = $node->title_method;
            $title  = static::$method();
        }

        return $title;
    }



}




