<?php

namespace App\Cells;


class BaseCell
{

    function __construct($views_path)
    {
        $this->views_path = $views_path;
    }

    function show()
    {}

    protected function view($view, $data = [], $mergeData = []) {
        $view = $this->views_path . '.' . $view;

        return view( $view, $data, $mergeData )->render();
    }

}
