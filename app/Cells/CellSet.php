<?php

namespace App\Cells;


class CellSet {


    function fetch($cell_path, $action=null, $options=[])
    {
        if ( is_array( $action ) )
        {
            $options = $action;
            $action  = null;
        }

        if ( empty ( $action ) )
        {
            $action = 'show';
        }


        $studly_path = collect( explode( '.', $cell_path ) )
            ->map( function ($item, $key) {
                return studly_case( $item );
            })
            ->implode( '\\' );

        $class_name = implode( '\\', [__NAMESPACE__, $studly_path, 'Cell', ] );

        $cell = new $class_name( $cell_path );

//        $options['cell_path'] = $studly_path;
        return $cell->$action( $options );

    }

}