<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CellServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
//        app('view')->composer('layouts.admin', function( $view )
//        {
//            $view->with([
//                'cells' => new \App\Cells\CellSet,
//            ]);
//        });

        app('view')->share('cells', ( new \App\Cells\CellSet ));

        $this->app['view']->addLocation( realpath(base_path('resources/cells')) );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind('cells', function()
//
//        {
//            return new CellSet;
//
//        });
    }

}