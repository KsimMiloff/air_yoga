<?php

namespace App\Lib\Hierarchy;


class Node {
    public function __construct($options)
    {
        $locale = \App::getLocale();

        foreach( $options as $attribute => $value )
        {
            $this->$attribute = $value;
        }

    }

    public function title()
    {
        if ($this->locale_scope)
        {
            $string_chain = implode( '.', $this->chain );
            return trans("{$this->locale_scope}.{$string_chain}");
        }

        return $this->title;
    }

    public function level() {
        $chain = $this->nornalize_chain( $this->chain );
        return count( $chain );
    }


    public function find_by_chain($chain = [])
    {

        $chain = $this->nornalize_chain( $chain );

        $id = array_shift($chain);
        $child_node = $this->child($id);

        if (count($chain) && $child_node)
        {
            return $child_node->find_by_chain( $chain );
        } else {
            return $child_node;
        }
    }


    public function find_each_by_chain($chain)
    {
        $chain = $this->nornalize_chain( $chain );

        $result = [];
        for ($i = count( $chain ); $i >= 1; $i--)
        {
            $chain = array_slice( $chain, 0, $i );
            $result[] = $this->find_by_chain($chain);
        }
        return array_reverse( $result );
    }


    public function in_same_chain($node_or_chain)
    {
        $node1 = is_array($node_or_chain) ? Tree::find_by_chain($node_or_chain): $node_or_chain;
        $node2 = $this;

        if (count( $node2->chain ) < count( $node1->chain ))
        {
            list($node1, $node2) = [$node2, $node1];
        }

        return count( array_diff($node1->chain, $node2->chain) ) ? false : true;
    }


    public function is_ancestor_of($node_or_chain)
    {
        $possible_descendant = is_array($node_or_chain) ? Tree::find_by_chain($node_or_chain): $node_or_chain;
        if ( $possible_descendant->level() <= $this->level() || ! $this->in_same_chain($possible_descendant) )
        {
            return false;
        }
        $dna_test_result = starts_with( $possible_descendant->path(), $this->path() );
        return $dna_test_result;
    }




    public function path()
    {
        return implode('/', $this->chain);
    }


    private function child($id)
    {
        return collect($this->branches)->filter(function($node) use ($id) {
           return $node->id == $id;
        })->first();
    }

    private function nornalize_chain($chain)
    {
        if (is_string($chain))
        {
            foreach( ['.', '/'] as $separator )
            {
                if ( strpos( $chain, $separator ) !== false )
                {
                    return explode( $separator, $chain );
                }
            }
            if (is_string( $chain ))
            {
                $chain = [$chain];
            }
        }

        return $chain;
    }
}