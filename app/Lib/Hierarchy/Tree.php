<?php
namespace App\Lib\Hierarchy;

use File;
use Parser;

use App\Lib\Singleton;


class Tree
{
    use Singleton;

    const LOCALE_SCOPE = null;
    const STRUCT_PATH  = null;

    private function __construct()
    {
        $filename     = config_path(static::STRUCT_PATH);
        $file_content = File::get($filename);

        $hash_tree  = Parser::yaml($file_content);
        $this->tree = $this->grow_up( $hash_tree );
    }


    public static function root() {
        return new Node( [
            'branches' => static::tree(),
            'chain' => [],
            'title' => '',
            'locale_scope' => static::LOCALE_SCOPE
        ] );
    }


    public static function find_by_chain($chain = [])
    {
        return static::root()->find_by_chain($chain);
    }


    public static function find_each_by_chain($chain = [])
    {
        return static::root()->find_each_by_chain($chain);
    }


    public static function path_list($node = null, $list=[])
    {
        if ($node)
        {
            $list[] = $node->path();
        }

        $node = $node ?: static::root();

        foreach ($node->branches as $node) {
            $list = array_merge( $list, static::path_list( $node ) );
        }

        return $list;

    }

    public static function path_collection()
    {
        return collect( static::path_list() );
    }


    private static function tree() {
        return static::getInstance()->tree;
    }


    private static function grow_up($tree, $chain=[])
    {

        $locale = \App::getLocale();
        $nodes = [];

        foreach( $tree as $id => $node )
        {
            $node['id'] = $id;
            $node['chain'] = $chain;
            $node['chain'][] = $id;
            $node['branches'] = static::subnodes( $node );

            if (static::LOCALE_SCOPE) {
                $node['locale_scope'] = static::LOCALE_SCOPE;
            }

            if (! isset($node['title'])) {
                $node['title'] = '';
            }

            $nodes[] = new Node($node);
        }

        return $nodes;
    }

    private static function subnodes($node)
    {

        $branches = array_get($node, 'branches', []);
        $chain    = array_get($node, 'chain', []);

        return new NodeList( $branches ? (static::grow_up( $branches, $chain )) : []);
    }

}

