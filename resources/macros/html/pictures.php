<?php

Html::macro('url_to_picture', function($value, $options=[]) {
    $options = collect($options);
    $params  = collect(['id' => $value]);
    $crop    = null;

    if ($options->has('size')) {
        if ( $options->has('crop') && $options->get('crop') ) {
            $crop = 'crop';
        }
        $route  = 'images.resized';
        $params = $params->merge(['size' => $options->get('size'), 'crop' => $crop]);

    } else {
        $route = 'images.show';
    }

    return route($route, $params->toArray());
});

Html::macro('link_to_picture', function($value, $title = null, $options=[]) {
    $attrs = '';
    foreach( $options as $key => $val) {
        $attrs = "{$attrs} {$key}='{$val}'";
    }
    $src = Html::url_to_picture($value, $options);
    return "<a href='{$src}' {$attrs} >$title</a>";

});


Html::macro('thumbed_link_to_picture', function($value, $link_options=[], $thumb_options=[]) {
    $thumb = Html::picture($value, $thumb_options);
    return Html::link_to_picture($value, $thumb, $link_options);
});


//Html::macro('thumbed_link_to_picture', function($value, $link_options=[], $thumb_options=[]) {
//    $thumb =  Html::picture($value, $thumb_options);
////    $thumb .= '<i class="icon-btn flaticon-zoom"></i>';
//    return Html::link_to_picture($value, $thumb, $link_options);
//});


Html::macro('picture', function($value, $options=[]) {
    $src = Html::url_to_picture($value, $options);
    $style = array_get($options, 'style', '');
    $klazz = array_get($options, 'class', '');

    $img_tag = Html::image($src, '', ['style' => $style, 'class' => $klazz]);

//    ,

    return $img_tag;

});


