<?php
    Form::macro('resource', function($resource, $options=[]) {

        $route        = array_pull($options, 'route', null);
        $route_params = array_pull($options, 'route_params', []);

        if ( $route ) {
            if ( $resource->exists ) {
                $route = "{$route}.update";
                $route_params = array_merge(['id' => $resource->id], $route_params);
                $method_field = Form::hidden('_method', "PATCH");
            } else {
                $route = "{$route}.store";
                $method_field = '';
            }

            $url = route($route, $route_params);
            array_set($options, 'url', $url);
        }

        $form_tag = Form::model($resource, $options);

        return "{$form_tag} {$method_field}";

    });