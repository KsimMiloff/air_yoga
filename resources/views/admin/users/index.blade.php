@extends('layouts.admin')


@section('content')

    <div class="row">

        <h1>Пользователи
            {!! link_to_route('admin.users.create', 'Добавить', [], ['class' => 'btn btn-primary dropdown-toggle']) !!}
        </h1>

        @if ($users->isEmpty())
            <p>Нет ни одного товара</p>

        @else

            <table class="table table-hover">
                <colgroup>
                    <col width="70px"/>
                </colgroup>
                <thead>
                <tr>
                    <th>email</th>
                    <th>Имя</th>
                </tr>
                </thead>
                <tbody>

                @foreach ($users as $user)
                    <tr>

                        <td> {!! link_to_route('admin.users.edit', $user->email, ['item_id' => $user->id]) !!} </td>
                        <td> {!! $user->name !!} </td>

                    </tr>

                @endforeach

                </tbody>
            </table>

        @endif

    </div>
@stop