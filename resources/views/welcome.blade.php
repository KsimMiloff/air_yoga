<!DOCTYPE html>
<html>
<head>
    <title>Title of the document</title>

    <link href="/assets/styles/reset.css" rel="stylesheet">
    <link href="/assets/styles/style.css" rel="stylesheet">
    <link href="/assets/styles/layout.css" rel="stylesheet">
</head>

<body>
<div class="wrapper layout">

    <header>
        <div class="logo">
            <a href="/">
                <img src="/images/logo.png"/>
            </a>
        </div>
        <div class="contacts">
            <div class="feedback"><a href="#">контакты</a></div>
            <div class="phone">+7 707 789-30-56</div>
            <div class="mail"><a href="#">info@airyoga.kz</a></div>
        </div>
    </header>
    <div class="topmenu-container">
        <nav class="topmenu">
            <li><a href="#">Мария Сорокина</a></li>
            <li><a href="#">Школа Йоги</a></li>
            <li><a href="#">Занятия</a></li>
            <li><a href="#">Магазин</a></li>
            <li><a href="#">Новости</a></li>
            <li><a href="#">Полезно знать</a></li>
            <li><a href="#">Партнеры</a></li>
        </nav>
    </div>

    <div class="content">
        <div class="welcome-content">
            <div class="welcome-left">
                <div class="welcome-blocks">
                    <div class="welcome-block big purple">
                        <div class="welcome-block-content">
                            <h2 class="h2">
                                <a href="#">Мария Сорокина</a>
                                <small>ведущий тренер по йоге в воздухе</small>
                            </h2>
                            <ul>
                                <li><a href="#">Индивидуальные занятия</a></li>
                                <li><a href="#">Расписания</a></li>
                                <li><a href="#">Обучение</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="welocme_news">
                    <h2 class="h2 yellow lined">Новости</h2>
                    <p><a href="#">Отчет с "Йога дня 2015"</a> прошедшего на катке "Медео"</p>
                    <p>Открыт набор на <a href="#">"Базовый курс"</a> по йоге в гамаках</p>
                    <p>В центре <a href="#">"Дома солнца"</a> в Астане, открылся класс йоги в гамаках, знакомьтесь с инструкторами</p>
                    <p>21 июня на катке "Медео" пройдет "Йога день", приходите на бесплатный мастер класс по йоге в гамаках</p>
                    <p>Новые <a href="#">модели гамаков</a> в нашем магазине</p>
                </div>
            </div>
            <div class="welcome-right">
                <div class="welcome-blocks">
                    <div class="welcome-block small purple">
                        <div class="welcome-block-content">
                            <h2 class="h2"><a href="#">Школа йоги</a></h2>
                            <ul>
                                <li><a href="#">Обучение тренеров</a></li>
                                <li><a href="#">Оборудование залов</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="welcome-block small purple">
                        <div class="welcome-block-content">
                            <h2 class="h2"><a href="#">Йога для детей</a></h2>
                            <i>занятия йогой в воздухе для детей от 6 до 12 лет</i>
                        </div>
                    </div>
                </div>
                <div class="welcome-blocks">
                    <div class="welcome-block small yellow">
                        <div class="welcome-block-content">
                            <h2 class="h2"><a href="#">Занятия</a></h2>
                            <ul>
                                <li><a href="#">Залы</a></li>
                                <li><a href="#">Расписание</a></li>
                                <li><a href="#">Йога для детей</a></li>
                                <li><a href="#">Реабилитация</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="welcome-block small purple">
                        <div class="welcome-block-content">
                            <h2 class="h2"><a href="#">Фотографии</a></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="gamacs">
        <div class="gamacs-title">
            <h2 class="gamacs-h">Гамаки</h2>
        </div>
        <div class="items">
            <div class="item">
                <div class="item-img"></div>
                <div class="item-title">
                    <a href="#">
                        Йога-гамак AirSwing Home
                    </a>
                </div>
                <div class="item-price">
                    23 000тг.
                </div>
            </div>
            <div class="item">
                <div class="item-img"></div>
                <div class="item-title">
                    <a href="#">
                        Йога-гамак AirSwing Home
                    </a>
                </div>
                <div class="item-price">
                    23 000тг.
                </div>
            </div>
            <div class="item">
                <div class="item-img"></div>
                <div class="item-title">
                    <a href="#">
                        Йога-гамак AirSwing Home
                    </a>
                </div>
                <div class="item-price">
                    23 000тг.
                </div>
            </div>
            <div class="item">
                <div class="item-img"></div>
                <div class="item-title">
                    <a href="#">
                        Йога-гамак AirSwing Home
                    </a>
                </div>
                <div class="item-price">
                    23 000тг.
                </div>
            </div>
            <div class="item">
                <div class="item-img"></div>
                <div class="item-title">
                    <a href="#">
                        Йога-гамак AirSwing Home
                    </a>
                </div>
                <div class="item-price">
                    23 000тг.
                </div>
            </div>
        </div>
    </div>

    <div class="partners">
        <h2 class="h2 yellow">Сертифицированные партенры</h2>

    </div>

</div>

<footer>
    <div class="wrapper">
        <div class="copyright">
            (C) 2014-2015 Центр развития "AirFit"
        </div>
        <div class="ip">
            ИП "Сорокина Мария Владимировна"
            <br/>
            Свидетельство № 07987655443
        </div>
    </div>
</footer>

</body>

</html>