<ul class="{!! isset( $class ) ? $class : 'dropdown-menu' !!}">
    @if ( count( $node->branches ) )

        @foreach($node->branches as $subnode)

            {!! $cells->fetch( 'admin.navbar', 'menu_item', $subnode ) !!}

        @endforeach

    @endif

</ul>
