<li class="{!! $li_class !!}">

    @if ($title)
        <a href="{!! ($url ? $url : '#' ) !!}" {!! $a_conf !!}>
            {!! $title !!}

            @if (count( $node->branches ) && $node->level() == 2)
                <span class="caret"></span>
            @endif
        </a>
    @endif

    @if (count( $node->branches ))
        @include('admin.cells.menu.tree', ['node' => $node])
    @endif

</li>