<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();


Route::group(['namespace' => 'File'], function()
{
    Route::post('files/wisiwyg_upload}',
        ['as' => 'files.wisiwyg_upload', 'uses' => 'FilesController@wisiwyg_upload']
    );

    Route::get('images/{id}/resized/{size}/{crop?}',
        ['as' => 'images.resized', 'uses' => 'ImagesController@resize']
    );

    Route::resource('images', 'ImagesController', ['except' => ['destroy']]);
    Route::resource('files', 'FilesController', ['except' => ['destroy']]);
});


Route::get('admin', function () {
    return redirect('/admin/users');
});



Route::group([ 'namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'auth' ], function()
{
    Route::get('/dashboard', 'DashboardController@index');

    // вот такой вот хак "группа в группе", почему-то лара5.3 перестала добавлять префикс "admin." в name рутинга
    Route::group([ 'as' => 'admin.' ], function() {
        Route::resource('users', 'UsersController', ['except' => ['show', 'destroy']]);

        Route::get('contents/create/{category_id?}', ['as' => 'admin.contents.create', 'uses' => 'ContentsController@create']);
        Route::resource('contents', 'ContentsController', ['except' => ['create']]);
    });



});


Route::get('/', 'WelcomeController@index');